<?php
declare(strict_types=1);

namespace MVQN\Common\Exceptions;

use Exception;

final class ArrayTraversalException extends Exception
{
}